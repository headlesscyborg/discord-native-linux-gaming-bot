// Load up the discord.js library
const Discord = require("discord.js");
const client = new Discord.Client();
const config = require("./config.json");
const goodnightwords2 = ["good night"];

function jokegenerator(){
var items = ["Q: How do you know Houtworm is sleeping? A: There is no one chatting on the NLG Discord", "There are two ways to write bug-free programs; only the third one works","The best thing about a Boolean is even if you are wrong, you are only off by a bit.","The best method for accelerating a computer is the one that boosts it by 9.8 m/s2","Did you hear about the restaurant on the moon? Great food, no atmosphere.","What do you call a fake noodle? An Impasta.","How many apples grow on a tree? All of them.", "Want to hear a joke about paper? Nevermind it's tearable.", "Why did the coffee file a police report? It got mugged.", "I just watched a program about beavers. It was the best dam program I've ever seen.", "What do you call a Mexican who has lost his car? Carlos.", "Dad, can you put my shoes on? No, I don't think they'll fit me.", "Why did the scarecrow win an award? Because he was outstanding in his field.", "Why don't skeletons ever go trick or treating? Because they have no body to go with.", "Windows 10", "This is the year of Linux desktop.", "2015 will be the year of Linux desktop", "Ill call you later. Don't call me later, call me Dad.", "What do you call an elephant that doesn't matter? An irrelephant", "Want to hear a joke about construction? I'm still working on it.", "What do you call cheese that isn't yours? Nacho Cheese.", "Why couldn't the bicycle stand up by itself? It was two tired.", "What did the grape do when he got stepped on? He let out a little wine.", "I wouldn't buy anything with velcro. It's a total rip-off.", "The shovel was a ground-breaking invention.", "This graveyard looks overcrowded. People must be dying to get in there.", "Whenever the cashier at the grocery store asks my dad if he would like the milk in a bag he replies No, just leave it in the carton!", "5/4 of people admit that they’re bad with fractions.", "Two goldfish are in a tank. One says to the other, do you know how to drive this thing?", "What do you call a man with a rubber toe? Roberto.", "What do you call a fat psychic? A four-chin teller.", "I would avoid the sushi if I was you. It’s a little fishy.", "To the man in the wheelchair that stole my camouflage jacket... You can hide but you can't run.", "The rotation of earth really makes my day.", "I thought about going on an all-almond diet. But that's just nuts", "What's brown and sticky? A stick", "I’ve never gone to a gun range before. I decided to give it a shot!", "Why do you never see elephants hiding in trees? Because they're so good at it.", "Did you hear about the kidnapping at school? It's fine, he woke up.", "A furniture store keeps calling me. All I wanted was one night stand.", "I used to work in a shoe recycling shop. It was sole destroying.", "Did I tell you the time I fell in love during a backflip? I was heels over head.", "I don’t play soccer because I enjoy the sport. I’m just doing it for kicks.", "People don’t like having to bend over to get their drinks. We really need to raise the bar.", "What’s the difference between a blonde and a mosquito? A mosquito stops sucking when you smack it.", "How is a pussy like a grapefruit? The best ones squirt when you eat them.", "What’s the difference between acne and a Catholic Priest? Acne will usually not come on a kid’s face until around 13 or 14 years of age.", "How do you turn a fox into an elephant? Marry it!", "Why does the bride always wear white? Because it is good for the dishwasher to match the stove and refrigerator.", "What is the difference between a battery and a woman? A battery has a positive side.", "How do you tell if a chick’s too fat to f*ck? When you pull her pants down and her ass is still in them.", "What is the difference between a drug dealer and a hooker? A hooker can wash her crack and sell it again!", "Do you know why they call it the Wonder Bra? When you take it off you wonder where her tits went.", "Why is it so hard for women to take a piss in the morning? Did you ever try to peel apart a grilled cheese sandwich?", "Why don’t pygmies wear tampons? They keep stepping on the strings.", "Why is it difficult to find men who are sensitive, caring and good looking? They’ve got boyfriends already.", "Why do men like blowjobs? It’s the only time they get something into a woman’s head straight!", "What’s worse than a cardboard box? Paper tits!", "Why do Jewish men like to watch porno movies backwards? They like the part where the hooker gives the money back.", "Wikipedia: I know everything! Google: I have everything! Facebook: I know everybody! Internet: Without me you are nothing! Electricity: Keep talking bitches!", "Girls are like Internet Domain names, the ones I like are already taken.", "A wife sent her husband an sms on a cold winter evening: Windows frozen. The husband sent back: Pour some warm water over them. Some time later the husband receives an answer from his wife: The computer is completely fucked now", "Where's the best place to hide a body? Page two of Google.", "A system administrator has 2 problems: 1) dumb users, 2)smart users","If at first you don't succeed, call it version 1.0.", "How do two programmers make money? One writes viruses, the other anti-viruses.", "The 21st century: Deleting history is more important than making it.", "My wifi suddenly stopped working then I realized that my neighbors have not paid the bill. How irresponsible some people they are.", "CAPS LOCK – Preventing Login Since 1980.", "Algorithm. Word used by programmers when they don't want to explain what they did.", "The Internet: where men are men, women are men, and children are the FBI...", "Programmer. A person who fixed a problem that you don't know you have, in a way you don't understand.", "To the optimist, the glass is half-full. To the pessimist, the glass is half-empty. To the IT professional, the glass is twice as big as it needs to be.", "I changed my password to incorrect. So whenever I forget what it is the computer will say Your password is incorrect.", "Funny facts about Google users: 50% of people use Google well as a search engine. The other 50% of them use it to check if their internet is connected", "Programmer. A machine that turns coffee into code.", "Hide all of the desktop icons on someone's computer and replace the monitor's wallpaper with a screen-shot of their desktop.", "Error, no keyboard. Press F1 to continue.", "Me: Siri, why am I alone? Siri: *opens front facing camera*", "Google+ is the gym of social networking. We all join, but nobody actually uses it.", "The code that is the hardest to debug is the code that you know cannot possibly be wrong.", "I had a programming problem and decided to use regular expressions to solve it. Now I have two problems.", "I'm not anti-social. I'm just not user friendly.", "A failure in a device will never appear until it has passed final inspection.", "How can you tell which one of your friends has the new iPhone X? Don't worry, they'll let you know.", "How do I know if someone is an Arch Linux user? Nah, don't worry, they will tell you.", "He didn't tell me his name yet because he was busy telling me how everyone should switch to Arch.", "My attitude isn't bad. It's in beta.", "I named my dog Sudo.", "Once upon a time, a computer programmer drowned at sea. Many were on the beach and heard him cry out, “F1! F1!”, but no one understood.", "A Man from the toilet shouts to his wife: Darling, darling, do you hear me?!!!! What happened, did you run out of toilet paper? No, restart the router, please!", "Yo mama is so stupid that she bought curtains for her computer just because it had Windows.", "I would love to change the world, but they won't give me the source code.", "Home is where the wifi connects automatically.", "Yo mama so stupid, the password needed 8 characters, so she put Snow white and the 7 dwarves.", "There are 10 types of people in the world: those who understand binary, and those who don't.", "A SEO couple had twins. For the first time they were happy with duplicate content.", "Why is it that programmers always confuse Halloween with Christmas? Because 31 OCT = 25 DEC", "In order to understand recursion you must first understand recursion.", "Why do Java developers wear glasses? Because they can't C#", "An SEO expert walks into a bar, bars, pub, tavern, public house, Irish pub, drinks, beer, alcohol", "If you put a million monkeys on a million keyboards, one of them will eventually write a Java program. The rest of them will write Perl programs. ", "There's a band called 1023MB. They haven't had any gigs yet. ", "There are only two hard things in computer science: cache invalidation, naming things, and off-by-one errors. ", "A programmer's wife asks him to pick up some groceries on his way home from work. He asks what she needs, and she says to pick up a gallon of milk, and if they have eggs, get a dozen. When he returns home, his wife asks why he brought home 12 gallons of milk, and he responded that they did indeed have eggs.", "I love pressing F5. It's so refreshing. ", "A password cracker walks into a bar. Orders a beer. Then a Beer. Then a BEER. beer. b33r. BeeR. Be3r. bEeR. bE3R. BeEr", "I ran out of new IPv4 jokes. I could tell you an IPv6 one but I'm afraid, you might not understand it.", "An ARP request goes to McDonald’s and asks for a Big MAC.", "Multicast jokes are awesome. But you'll get them only if you bother to listen.", "The worst thing about a broadcast joke is that you have to tell it to everyone in order to find the one person who gets it. ", "Five routers walk into a bar. One of the routers goes up to the bartender and asks for four jack and cokes, and one seltzer with lime. A drunk patron overheads, laughs, and asks the router, Who’s the seltzer for? I’m the designated router, he replies.", "A dyslexic man walks into a bra.", "Why aren’t koalas actual bears? The don’t meet the koalafications.", "My friend thinks he is smart. He told me an onion is the only food that makes you cry, so I threw a coconut at his face. ", "What happens to a frog's car when it breaks down? It gets toad away. ", "Q: Is Google male or female? A: Female, because it doesn't let you finish a sentence before making a suggestion. ", "Q: What did the duck say when he bought lipstick? A: Put it on my bill. ", "Q: Why was six scared of seven? A: Because seven 'ate' nine. ", "Q: Can a kangaroo jump higher than the Empire State Building? A: Of course. The Empire State Building can't jump. ", "A boy asks his father, 'Dad, are bugs good to eat?' 'That's disgusting. Don't talk about things like that over dinner,' the dad replies. After dinner the father asks, 'Now, son, what did you want to ask me?' 'Oh, nothing,' the boy says. 'There was a bug in your soup, but now it’s gone.'", "Q: Why couldn't the blonde add 10 + 5 on a calculator? A: She couldn't find the '10' button. ", "In a Catholic school cafeteria, a nun places a note in front of a pile of apples, 'Only take one. God is watching.' Further down the line is a pile of cookies. A little boy makes his own note, 'Take all you want. God is watching the apples.'", "A bus full of ugly people had a head on collision with a truck. When they died, God granted all of them one wish. The first person said, 'I want to be gorgeous.' God snapped his fingers and it happened. The second person said the same thing and God did the same thing. This want on and on throughout the group. God noticed the last man in line was laughing hysterically. By the time God got to the last ten people, the last man was laughing and rolling on the ground. When the man's turn came, he laughed and said, 'I wish they were all ugly again.' ", "Q: Why did the witches' team lose the baseball game? A: Their bats flew away. ", "A husband and wife were driving through Louisiana. As they approached Natchitoches, they started arguing about the pronunciation of the town. They argued back and forth, then they stopped for lunch. At the counter, the husband asked the blonde waitress, 'Before we order, could you please settle an argument for us? Would you please pronounce where we are very slowly?' She leaned over the counter and said, 'Burrr-gerrr Kiiing.' ", "There was an elderly couple who in their old age noticed that they were getting a lot more forgetful, so they decided to go to the doctor. The doctor told them that they should start writing things down so they don't forget. They went home and the old lady told her husband to get her a bowl of ice cream. 'You might want to write it down,' she said. The husband said, 'No, I can remember that you want a bowl of ice cream.' She then told her husband she wanted a bowl of ice cream with whipped cream. 'Write it down,' she told him, and again he said, 'No, no, I can remember: you want a bowl of ice cream with whipped cream.' Then the old lady said she wants a bowl of ice cream with whipped cream and a cherry on top. 'Write it down,' she told her husband and again he said, 'No, I got it. You want a bowl of ice cream with whipped cream and a cherry on top.' So he goes to get the ice cream and spends an unusually long time in the kitchen, over 30 minutes. He comes out to his wife and hands her a plate of eggs and bacon. The old wife stares at the plate for a moment, then looks at her husband and asks, 'Where's the toast?' ", "Late one night a burglar broke into a house and while he was sneaking around he heard a voice say, 'Jesús is watching you.' He looked around and saw nothing. He kept on creeping and again heard, 'Jesús is watching you.' In a dark corner, he saw a cage with a parrot inside. The burglar asked the parrot, 'Was it you who said Jesús is watching me' The parrot replied, 'Yes.' Relieved, the burglar asked, 'What is your name?' The parrot said, 'Clarence.' The burglar said, 'That's a stupid name for a parrot. What idiot named you Clarence?' The parrot answered, 'The same idiot that named the rottweiler Jesús.' ", "A proud and confident genius makes a bet with an idiot. The genius says, 'Hey idiot, every question I ask you that you don't know the answer, you have to give me $5. And if you ask me a question and I can't answer yours I will give you $5,000.' The idiot says, 'Okay.' The genius then asks, 'How many continents are there in the world?' The idiot doesn't know and hands over the $5. The idiot says, 'Now me ask: what animal stands with two legs but sleeps with three?' The genius tries and searches very hard for the answer but gives up and hands over the $5000. The genius says, 'Dang it, I lost. By the way, what was the answer to your question?' The idiot hands over $5. ", "During lunch at work, I ate 3 plates of beans (which I know I shouldn't). When I got home, my husband seemed excited to see me and exclaimed delightedly, 'Darling I have a surprise for dinner tonight.' He then blindfolded me and led me to my chair at the dinner table. I took a seat and just as he was about to remove my blindfold, the telephone rang. He made me promise not to touch the blindfold until he returned and went to answer the call. The beans I had consumed were still affecting me and the pressure was becoming unbearable, so while my husband was out of the room I seized the opportunity, shifted my weight to one leg and let one go. It was not only loud, but it smelled like a fertilizer truck running over a skunk in front of a garbage dump! I took my napkin from my lap and fanned the air around me vigorously. Then, shifting to the other leg, I ripped off three more. The stink was worse than cooked cabbage. Keeping my ears carefully tuned to the conversation in the other room, I went on releasing atomic bombs like this for another few minutes. The pleasure was indescribable! Eventually the telephone farewells signaled the end of my freedom, so I quickly fanned the air a few more times with my napkin, placed it on my lap and folded my hands back on it feeling very relieved and pleased with myself. My face must have been the picture of innocence when my husband returned, apologizing for taking so long. He asked me if I had peaked through the blindfold, and I assured him I had not. At this point, he removed the blindfold, and twelve dinner guests seated around the table, with their hands to their noses, chorused, 'Happy Birthday!' ","A very old woman realizes that she's seen and done everything and the time has come to depart from this world. After considering various methods of doing away with herself, she decides to shoot herself through the heart. Not wanting to make a mistake, she phones her doctor and asks him the exact location of the heart. He tells her that the heart is located two inches below the left nipple. The old woman hangs up the phone, takes careful aim and shoots herself in the left knee.","A young Programmer and his Project Manager board a train headed through the mountains on its way to Wichita. They can find no place to sit except for two seats right across the aisle from a young woman and her grandmother. After a while, it is obvious that the young woman and the young programmer are interested in each other, because they are giving each other looks. Soon the train passes into a tunnel and it is pitch black. There is a sound of a kiss followed by the sound of a slap. When the train emerges from the tunnel, the four sit there without saying a word. The grandmother is thinking to herself, 'It was very brash for that young man to kiss my granddaughter, but I'm glad she slapped him.' The Project manager is sitting there thinking, 'I didn't know the young tech was brave enough to kiss the girl, but I sure wish she hadn't missed him when she slapped me!' The young woman was sitting and thinking, 'I'm glad the guy kissed me, but I wish my grandmother had not slapped him!' The young programmer sat there with a satisfied smile on his face. He thought to himself, 'Life is good. How often does a guy have the chance to kiss a beautiful girl and slap his Project manager all at the same time!'","A man was crossing a road one day when a frog called out to him and said, 'If you kiss me, I'll turn into a beautiful princess.' He bent over, picked up the frog, and put it in his pocket. The frog spoke up again and said, 'If you kiss me and turn me back into a beautiful princess, I will tell everyone how smart and brave you are and how you are my hero' The man took the frog out of his pocket, smiled at it, and returned it to his pocket. The frog spoke up again and said, 'If you kiss me and turn me back into a beautiful princess, I will be your loving companion for an entire week.' The man took the frog out of his pocket, smiled at it, and returned it to his pocket. The frog then cried out, 'If you kiss me and turn me back into a princess, I'll stay with you for a year and do ANYTHING you want.' Again the man took the frog out, smiled at it, and put it back into his pocket. Finally, the frog asked, 'What is the matter? I've told you I'm a beautiful princess, that I'll stay with you for a year and do anything you want. Why won't you kiss me?' The man said, 'Look, I'm a computer programmer. I don't have time for a girlfriend, but a talking frog is cool.'"];

  var rnd = Math.floor(Math.random()*items.length);
  var output = items[rnd];
  return output;
}

client.on("ready", () => {
console.log(`[${today()}]: bot started`); 
client.user.setActivity(`Arch Linux x64`);
});

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// This is a timer trigger, currently disabled but it can repeat an action at specific time
// Example: event reminder

//event_tombraider
//setInterval(function() {
//console.log(`[${today()}]: event_tombraider, checking date and time`); 
//if (today().startsWith("Sat")) {
//console.log(`[${today()}]: event_tombraider, day ok`); 
//console.log(`[${today()}]: event_tombraider, checking time`); 
//if (today().includes("18:30:")) {
//console.log(`[${today()}]: event_tombraider, time ok`); 
////if (today().startsWith("Mon")) {
////if (today().includes("17:05:")) {
//console.log(`[${today()}]: event_tombraider, getting channel ID`); 
//var channel = client.channels.get('444848980673953792');
//console.log(`[${today()}]: event_tombraider, sending message to the 
//-looking-for-players- channel`); 
//channel.send("**Tomb Raider** multiplayer event starts in 30 minutes! (7:00 PM | 19:00 
//| UTC+2)");
//console.log(`[${today()}]: event_tombraider, message sent`); 
//}
//if (today().includes("19:00:")) {
//console.log(`[${today()}]: event_tombraider, second requirement met - getting channel 
//ID`); 
//var channel = client.channels.get('444848980673953792');
//console.log(`[${today()}]: event_tombraider, sending message to the 
//-looking-for-players- channel`); 
//channel.send("**Tomb Raider** multiplayer event is starting now!");
//console.log(`[${today()}]: event_tombraider, message sent`); 
//}}}, 45 * 1000);

function today(){
var thisday = new Date();
thisday.setDate;
return `${thisday}`;
}

function aweeklater(){
console.log(`[${today()}]: function called - aweeklater()`); 
var oneweeklater = new Date();
oneweeklater.setDate(oneweeklater.getDate() + 7);
console.log(`[${today()}]: function returning - aweeklater()`); 
return `${oneweeklater}`;
}

client.on("message", async message => {
if(message.author.bot) return;
const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
const command = args.shift().toLowerCase();

if(command === "joke") {
console.log(`[${today()}]: command called - joke`);
if(message.content.indexOf(config.prefix) !== 0) return;
message.channel.send(jokegenerator());
console.log(`[${today()}]: command replying - joke`); 
}

if(command === "tell") {
console.log(`[${today()}]: command called - tell`); 
if(message.content.indexOf(config.prefix) !== 0) return;
if(!message.member.roles.some(r=>["tuxbot"].includes(r.name)) ) // "tuxbot" is used as the role required to use bot specific commands
return message.reply("Sorry, you don't have permissions to use this!");
const sayMessage = args.join(" ");
message.channel.send(sayMessage);
console.log(`[${today()}]: command replying - tell`); 
  }

//Administrator commands
if(command === "kick") {
console.log(`[${today()}]: command called - kick`); 
if(message.content.indexOf(config.prefix) !== 0) return;
if(!message.member.roles.some(r=>["tuxbot"].includes(r.name)) )
return message.reply("Sorry, you don't have permissions to use this!");
let member = message.mentions.members.first() || message.guild.members.get(args[0]);
if(!member)
return message.reply("Please mention a valid member of this server");
if(!member.kickable) 
return message.reply("This user cannot be kicked. Please ask the server owner for more information,");
let reason = args.slice(1).join(' ');
if(!reason) reason = "No reason provided.";
await member.kick(reason)
.catch(error => message.reply(`Sorry ${message.author} I couldn't kick because of : ${error}`));
message.reply(`${member.user.tag} has been kicked by ${message.author.tag} because: ${reason}`);
}

if (command === "today") {
if(message.content.indexOf(config.prefix) !== 0) return;
return message.reply(today());
}

function goodnight_generator(){
var items = ["good night.", "good night to you too.", "sweet dreams.", "nighty night!", "go to bead, you sleepy head", "sleep tight!", "sleep snug as a bug in a rug.", "see ya' in the morning.", "I will be dreaming of you.", "bedtime for you, good night.", "lights out!"];
var rnd = Math.floor(Math.random()*items.length);
var output = items[rnd];
return output;
}

if(goodnightwords2.some(word => message.content.includes(word))) {
message.reply(goodnight_generator());
}

if (command === "aweeklater") {
if(message.content.indexOf(config.prefix) !== 0) return;
return message.reply(aweeklater());}

if (command === "quitbot") {
if(message.content.indexOf(config.prefix) !== 0) return;
if(!message.member.roles.some(r=>["tuxbot"].includes(r.name)) )
     return message.reply("Sorry, you don't have permissions to use this!");
		return message.reply("shutdown -h now && goodbye");
		client.destroy();
		process.exit();}
  
  if(command === "ban") {
if(message.content.indexOf(config.prefix) !== 0) return;
    if(!message.member.roles.some(r=>["tuxbot"].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
    let member = message.mentions.members.first();
    if(!member)
      return message.reply("Please mention a valid member of this server");
    if(!member.bannable) 
      return message.reply("I cannot ban this user!");
    let reason = args.slice(1).join(' ');
    if(!reason) reason = "No reason provided";
    await member.ban(reason)
      .catch(error => message.reply(`Sorry ${message.author} I couldn't ban because of : ${error}`));
    message.reply(`${member.user.tag} has been banned by ${message.author.tag} because: ${reason}`);}

  if(command === "purge") {
if(message.content.indexOf(config.prefix) !== 0) return;
  if(!message.member.roles.some(r=>["tuxbot"].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");  
    const deleteCount = parseInt(args[0], 10);
    if(!deleteCount || deleteCount < 2 || deleteCount > 100)
      return message.reply("Please provide a number between 2 and 100 for the number of messages to delete");
    const fetched = await message.channel.fetchMessages({limit: deleteCount});
    message.channel.bulkDelete(fetched)
      .catch(error => message.reply(`Couldn't delete messages because of: ${error}`));
  }

if(command === "mpseek") {
if(message.content.indexOf(config.prefix) !== 0) return;
if(message.member.roles.find("name", "mpseek")){
const guildMember = message.member;
const role = message.guild.roles.find('name', 'mpseek');
guildMember.removeRole(role);
message.reply("you have been removed from the **mpseek** role.");  
} else {
const guildMember = message.member;
const role = message.guild.roles.find('name', 'mpseek');
guildMember.addRole(role);
message.reply("you have been added to the **mpseek** role."); 
}}

if(command === "beyondnative") {
if(message.content.indexOf(config.prefix) !== 0) return;
if(message.member.roles.find("name", "beyondnative")){
const guildMember = message.member;
const role = message.guild.roles.find('name', 'beyondnative');
guildMember.removeRole(role);
message.reply("you have been removed from the **beyondnative** role.");  
} else {
const guildMember = message.member;
const role = message.guild.roles.find('name', 'beyondnative');
guildMember.addRole(role);
message.reply("you have been added to the **beyondnative** role."); 
}}

if (command === "commands") {
if(message.content.indexOf(config.prefix) !== 0) return;
return message.reply("\n**Server commands:**\n```!rules - prints rules in any channel\n!proton - answers to common Proton questions\n!colors - information about colors and roles\n!lue - link to the LUE Steam group\n!calladmin - sends a notification to server administrators\n!randomdistro - generates a message with random Linux distribution\n!mpseek - adds/removes your account to/from the mpseek group\n!levels - information about server ranks, XP and levels\n!report - sends a notification to a private channel only visible to administrators and moderators of this server and deletes the original command message, this command allows users to secrectly report suspicious behaviour (your command will only be visible for 0.5-1.0 second - bot reaction time)```");
}

if (command === "rules") {
if(message.content.indexOf(config.prefix) !== 0) return;
return message.reply("\n**Server rules:**\n```1. This Discord server is dedicated to native Linux gaming. Wine, DXVK, VFIO, Proton etc. treated as beyond off-topic. Games released officially for Linux using compatibility layers are treated as native. You are completely free to use Proton, even some of use contribute to ProtonDB but we don't talk about it here.\n2. Do not link or request pirated material.\n3. Sending/Linking any harmful material such as viruses, IP grabbers or harmware is not tolerated.\n4. Communication should be in English.\n5. Non Linux-exclusive users or even non-Linux users are free to to be members of this server as long as they stay on topic (Native Linux gaming), this server has been created as a meeting place for native Linux-exclusive users who are not interested in compatibility layers or other platforms. In other words  - we welcome everyone who is interested in gaming on Linux natively, but stay on topic.\n6. Do not use overuse CAPITAL LETTERS.```");
}

if (command === "proton") {
if(message.content.indexOf(config.prefix) !== 0) return;
return message.reply("\nThanks for your interest. Proton is not allowed on this server because we would like to dedicated it to native Linux games and supporting Linux developers/publishers. It doesn't mean you are not welcome here! We will be happy if you stay with us but we just ask you to follow server rules. Howewer we have hidden channels where you can talk about about Proton, use the '!beyondnative' command to enable this special category.");
}

if (command === "admins") {
if(message.content.indexOf(config.prefix) !== 0) return;
return message.reply("\n**Server administrators:**\n\ncbones\ncprn\nheadless_cyborg");
}

if (command === "calladmin") {
if(message.content.indexOf(config.prefix) !== 0) return;
return message.channel.send("I think you should see this <@&444580774935658516> !");
var channel = client.channels.get('484810431341395968'); //channelID available if developer mode is enabled in Discord settings, then right click on any channel
  channel.sendMessage("**Someone called administrators**");
}

if (command === "lue") {
if(message.content.indexOf(config.prefix) !== 0) return;
return message.reply("\n**LUE information:**\n\nAnyone who is interested can request joining our Linux Users Exclusively group on Steam - <https://steamcommunity.com/groups/LinuxUsersExclusively> - please make sure you review LUE Membership Rules and Admission Requirements here: <https://steamcommunity.com/groups/LinuxUsersExclusively/discussions/0/1736589519997395428/>");
}

});

client.on('guildMemberRemove', member => {
    member.guild.channels.get('444484424281620502').send("<@" + member.id + "> has left the server."); 
});

client.on('guildMemberAdd', member => {
    member.guild.channels.get('556136861475536906').send("Hello <@" + member.id + "> , welcome to the Native Linux Gaming Discord server!");

// QUARANTINE

//member.guild.channels.get('444484424281620502').send("<@" + member.id + ">  has joined the server waiting in **welcome-channel** to be approved. Go and say hello to them :slight_smile:"); 
//const role = member.guild.roles.find('name', 'notapproved');
// We use permission-based protection against bots and trolls.  In order to get full access to the server, please introduce yourself or tell us why you joined us. Thanks for your understanding :slight_smile:
//member.addRole(role); 
});

client.login(config.token);
